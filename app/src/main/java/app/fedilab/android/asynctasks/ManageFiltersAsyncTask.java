/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

import app.fedilab.android.client.API;
import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.Entities.Filters;
import app.fedilab.android.interfaces.OnFilterActionInterface;


/**
 * Created by Thomas on 05/09/2018.
 * Async works to manage Filters
 */

public class ManageFiltersAsyncTask {

    private final OnFilterActionInterface listener;
    private final action apiAction;
    private final WeakReference<Context> contextReference;
    private final Filters filter;
    private APIResponse apiResponse;
    private int statusCode;

    public ManageFiltersAsyncTask(Context context, action apiAction, Filters filter, OnFilterActionInterface onFilterActionInterface) {
        contextReference = new WeakReference<>(context);
        this.listener = onFilterActionInterface;
        this.filter = filter;
        this.apiAction = apiAction;
        doInBackground();
    }

    protected void doInBackground() {

        new Thread(() -> {
            if (apiAction == action.GET_ALL_FILTER) {
                apiResponse = new API(contextReference.get()).getFilters();
            } else if (apiAction == action.GET_FILTER) {
                apiResponse = new API(contextReference.get()).getFilters(filter.getId());
            } else if (apiAction == action.CREATE_FILTER) {
                apiResponse = new API(contextReference.get()).addFilters(filter);
            } else if (apiAction == action.UPDATE_FILTER) {
                apiResponse = new API(contextReference.get()).updateFilters(filter);
            } else if (apiAction == action.DELETE_FILTER) {
                statusCode = new API(contextReference.get()).deleteFilters(filter);
            }
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onActionDone(this.apiAction, apiResponse, statusCode);
            mainHandler.post(myRunnable);
        }).start();
    }

    public enum action {
        GET_FILTER,
        GET_ALL_FILTER,
        CREATE_FILTER,
        DELETE_FILTER,
        UPDATE_FILTER,
    }

}
