/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import app.fedilab.android.R;
import app.fedilab.android.asynctasks.RetrieveFeedsAsyncTask;
import app.fedilab.android.asynctasks.UpdateAccountInfoAsyncTask;
import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.Entities.Status;
import app.fedilab.android.client.Entities.StatusDrawerParams;
import app.fedilab.android.client.Entities.StoredStatus;
import app.fedilab.android.databinding.ActivityHashtagBinding;
import app.fedilab.android.drawers.PixelfedListAdapter;
import app.fedilab.android.drawers.StatusListAdapter;
import app.fedilab.android.helper.Helper;
import app.fedilab.android.interfaces.OnRetrieveFeedsInterface;
import app.fedilab.android.sqlite.SearchDAO;
import app.fedilab.android.sqlite.Sqlite;
import es.dmoral.toasty.Toasty;


/**
 * Created by Thomas on 27/05/2017.
 * Show hashtag stream
 */

public class HashTagActivity extends BaseActivity implements OnRetrieveFeedsInterface {


    public static int position;
    private StatusListAdapter statusListAdapter;
    private PixelfedListAdapter pixelfedListAdapter;
    private String max_id;
    private List<Status> statuses;
    private boolean firstLoad;
    private String tag;
    private int tootsPerPage;
    private boolean flag_loading = false;
    private ActivityHashtagBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences sharedpreferences = getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
        int theme = sharedpreferences.getInt(Helper.SET_THEME, Helper.THEME_DARK);
        switch (theme) {
            case Helper.THEME_LIGHT:
                setTheme(R.style.AppTheme_NoActionBar_Fedilab);
                break;
            case Helper.THEME_BLACK:
                setTheme(R.style.AppThemeBlack_NoActionBar);
                break;
            default:
                setTheme(R.style.AppThemeDark_NoActionBar);
        }

        binding = ActivityHashtagBinding.inflate(getLayoutInflater());
        View viewRoot = binding.getRoot();
        setContentView(viewRoot);
        setSupportActionBar(binding.toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            tag = b.getString("tag", null);
        }
        if (tag == null)
            finish();
        statuses = new ArrayList<>();
        max_id = null;
        flag_loading = true;
        firstLoad = true;
        boolean isOnWifi = Helper.isOnWIFI(HashTagActivity.this);
        int c1 = getResources().getColor(R.color.cyanea_accent);
        int c2 = getResources().getColor(R.color.cyanea_primary_dark);
        int c3 = getResources().getColor(R.color.cyanea_primary);
        binding.swipeContainer.setProgressBackgroundColorSchemeColor(c3);
        binding.swipeContainer.setColorSchemeColors(
                c1, c2, c1
        );

        if (MainActivity.social == UpdateAccountInfoAsyncTask.SOCIAL.PIXELFED) {
            binding.toot.hide();
        }

        binding.toot.setOnClickListener(v -> {
            Intent intentToot = new Intent(HashTagActivity.this, TootActivity.class);
            Bundle val = new Bundle();
            StoredStatus storedStatus = new StoredStatus();
            Status tagStatus = new Status();
            tagStatus.setVisibility("public");
            tagStatus.setContent(HashTagActivity.this, String.format("#%s ", tag));
            storedStatus.setStatus(tagStatus);
            val.putParcelable("storedStatus", storedStatus);
            intentToot.putExtras(val);
            startActivity(intentToot);
        });

        binding.toot.setBackgroundColor(ContextCompat.getColor(HashTagActivity.this, R.color.cyanea_primary));

        tootsPerPage = sharedpreferences.getInt(Helper.SET_TOOT_PER_PAGE, Helper.TOOTS_PER_PAGE);
        StatusDrawerParams statusDrawerParams = new StatusDrawerParams();
        statusDrawerParams.setType(RetrieveFeedsAsyncTask.Type.TAG);
        statusDrawerParams.setTargetedId(null);
        statusDrawerParams.setOnWifi(isOnWifi);
        statusDrawerParams.setStatuses(this.statuses);

        if (MainActivity.social != UpdateAccountInfoAsyncTask.SOCIAL.PIXELFED) {
            statusListAdapter = new StatusListAdapter(statusDrawerParams);
            binding.lvStatus.setAdapter(statusListAdapter);
        } else {
            pixelfedListAdapter = new PixelfedListAdapter(statusDrawerParams);
            binding.lvStatus.setAdapter(pixelfedListAdapter);
        }

        setTitle(String.format("#%s", tag));
        binding.swipeContainer.setOnRefreshListener(() -> {
            max_id = null;
            statuses = new ArrayList<>();
            firstLoad = true;
            flag_loading = true;
            new RetrieveFeedsAsyncTask(HashTagActivity.this, RetrieveFeedsAsyncTask.Type.TAG, tag, null, max_id, HashTagActivity.this);
        });
        final LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(this);
        binding.lvStatus.setLayoutManager(mLayoutManager);
        binding.lvStatus.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                    if (firstVisibleItem + visibleItemCount == totalItemCount) {
                        if (!flag_loading) {
                            flag_loading = true;
                            new RetrieveFeedsAsyncTask(HashTagActivity.this, RetrieveFeedsAsyncTask.Type.TAG, tag, null, max_id, HashTagActivity.this);

                            binding.loadingNextStatus.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.loadingNextStatus.setVisibility(View.GONE);
                    }
                }
            }
        });
        new RetrieveFeedsAsyncTask(HashTagActivity.this, RetrieveFeedsAsyncTask.Type.TAG, tag, null, max_id, HashTagActivity.this);

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(@NotNull Menu menu) {
        getMenuInflater().inflate(R.menu.tag_pin, menu);

        SQLiteDatabase db = Sqlite.getInstance(getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
        List<String> searchInDb = new SearchDAO(HashTagActivity.this, db).getSearchByKeyword(tag.trim());
        if (searchInDb != null && searchInDb.size() > 0 || MainActivity.social == UpdateAccountInfoAsyncTask.SOCIAL.PIXELFED) {
            menu.findItem(R.id.action_pin).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();
            return true;
        } else if (itemId == R.id.action_pin) {
            SQLiteDatabase db = Sqlite.getInstance(getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
            new SearchDAO(HashTagActivity.this, db).insertSearch(tag);
            Intent intent = new Intent(HashTagActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Helper.INTENT_ACTION, Helper.SEARCH_TAG);
            intent.putExtra(Helper.SEARCH_KEYWORD, tag);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRetrieveFeeds(APIResponse apiResponse) {

        binding.loader.setVisibility(View.GONE);
        binding.loadingNextStatus.setVisibility(View.GONE);
        if (apiResponse == null || apiResponse.getError() != null) {
            if (apiResponse != null)
                Toasty.error(HashTagActivity.this, apiResponse.getError().getError(), Toast.LENGTH_LONG).show();
            else
                Toasty.error(HashTagActivity.this, getString(R.string.toast_error), Toast.LENGTH_LONG).show();
            return;
        }
        List<Status> statuses = apiResponse.getStatuses();
        if (firstLoad && (statuses == null || statuses.size() == 0))
            binding.noAction.setVisibility(View.VISIBLE);
        else
            binding.noAction.setVisibility(View.GONE);
        if (statuses != null && statuses.size() > 1)
            max_id = statuses.get(statuses.size() - 1).getId();
        else
            max_id = null;
        if (statuses != null) {
            this.statuses.addAll(statuses);
            if (statusListAdapter != null) {
                statusListAdapter.notifyDataSetChanged();
            } else if (pixelfedListAdapter != null) {
                pixelfedListAdapter.notifyDataSetChanged();
            }
        }
        binding.swipeContainer.setRefreshing(false);
        firstLoad = false;
        flag_loading = statuses != null && statuses.size() < tootsPerPage;
    }

}
